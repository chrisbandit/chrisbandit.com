from flask import Flask, render_template, request, jsonify, url_for
app = Flask(__name__)
app.config["DEBUG"] = True
import json
from views import *


if __name__ == "__main__":
    app.run(host='0.0.0.0')

